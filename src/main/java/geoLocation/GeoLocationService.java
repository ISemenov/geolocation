package geoLocation;

import com.fasterxml.jackson.core.JsonFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

@Service
public class GeoLocationService {

    @Autowired
    RestTemplate restTemplate;

    public Location getLocation(String address) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        httpHeaders.add("Accept", "application/json");
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Token 78012512e55ed777bdd15849cb55ab9f40bdb744");
        httpHeaders.add("X-Secret", "026332015ca72a8c6a679e3fab99c7d8b2b14b6d");

        String[] addresses = new String[]{address};
        HttpEntity<Serializable> request = new HttpEntity<>(addresses, httpHeaders);
        System.out.println(request);

        Location[] locations = restTemplate.postForObject("https://dadata.ru/api/v2/clean/address", request, Location[].class);

        return locations[0];
    }
}
