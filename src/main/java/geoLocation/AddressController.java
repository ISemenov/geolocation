package geoLocation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("geoLocation")
public class AddressController {

    @Autowired
    GeoLocationService geoLocationService;

    @GetMapping
    public String geoLocationForm(@ModelAttribute(name = "userRequest") UserRequest userRequest) {
        return "geoLocationForm";
    }

    @PostMapping
    public String geoLocationFormSubmit(@ModelAttribute UserRequest userRequest, Model model) {
        model.addAttribute("location", geoLocationService.getLocation(userRequest.getAddress()));
        return "coordinates";
    }
}
